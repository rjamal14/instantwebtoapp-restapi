var controllers_router = function(){
  const route = '/api/v1';
  const route_path = appDir + '/app/controllers/api/v1';

  var auth = require(route_path+'/Auth/AuthControllers');
  app.use(route+'/oauth', auth);

  var profile = require(route_path+'/Profile/ProfileControllers');
  app.use(route+'/profile', profile);

  var MasterApp = require(route_path+'/MasterApp/MasterAppControllers');
  app.use(route+'/MasterApp', MasterApp);

  var AppBuild = require(route_path+'/AppBuild/AppBuildControllers');
  app.use(route+'/AppBuild', AppBuild);
}

module.exports = controllers_router;