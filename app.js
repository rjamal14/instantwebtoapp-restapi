var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var path = require('path');
var jwt = require('jsonwebtoken');
var fs = require('fs');
var nodemailer = require('nodemailer');
var sliderCaptcha = require('@slider-captcha/core');
require('dotenv').config();

process.env.TZ = 'Asia/Jakarta'
global.app = express();
global.appDir = path.resolve(__dirname);

// cors
var cors = require('cors');
app.use(cors({
  origin: ['*'],
  credentials: true
}));

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Headers', true);
  res.header('Access-Control-Allow-Credentials', "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Origin");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  next();
});

app.use(logger('dev'));
app.use(cookieParser());
app.use(express.json({limit: '20mb'}));
app.use(express.urlencoded({limit: '20mb', extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/captcha/create', function (req, res) {
  sliderCaptcha.create()
    .then(function ({data, solution}) {
      req.session.captcha = solution;
      req.session.save();
      res.status(200).send(data);
    });
});

app.post('/captcha/verify', function (req, res) {
  sliderCaptcha.verify(req.session.captcha, req.body)
    .then(function (verification) {
      if (verification.result === 'success') {
        req.session.token = verification.token;
        req.session.save();
      }
      res.status(200).send(verification);
    });
});

//encrypt
const Cryptr = require('cryptr');
const cryptr = new Cryptr('RAJ1411UMKM');
global.encrypt = (obj) => {
  const encryptedString = cryptr.encrypt(obj);
  return encryptedString
}

global.decrypt = (obj) => {
  const decryptedString = cryptr.decrypt(obj);
  return decryptedString
}

//middleware authenticateJWT
global.auth = require(appDir+'/app/middleware/Auth');

// model & controllers
global.model = require(appDir+'/app/models/index');
var controllers_router = require(appDir+'/config/routes/index');
controllers_router();

let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.SMTP_MAIL,
      pass: process.env.SMTP_PASS // naturally, replace both with your real credentials or an application-specific password
    }
});


global.SendMail = async(info) =>{
  transporter.sendMail({
      from: 'instantwebtoapp@gmail.com',
      to: info.to,
      subject: 'Instantwebtoapp | '+info.subject,
      html: info.html,
  });
}

// upload images
global.uploadImage = async (image,type,id) => {
  try {
    const imgdata = image;
    const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
    const extension = imgdata.match(/[^:/]\w+(?=;|,)/)[0];
    const path = './public/images/'+type+"-"+id+"."+extension
    await fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
    return path.replace('./public/', '/');
  } catch (e) {
    console.error(e)
    return false
  }
}

// delete image
global.deleteImage = async (image) => {
  try {
    await fs.unlink('./public/'+image, (err) => {
      if (err) throw err;
      console.log(`${image} was deleted`);
    });
    return true
  } catch (e) {
    log.error(e)
    return false
  }
}

global.LibUsers = require(appDir+'/app/lib/LibUsers');


// var payment = async()=>{
//   var paypal = require('paypal-node-sdk');

//   paypal.configure({
//     'mode': 'sandbox', //sandbox or live
//     'client_id': 'AbYPK_UKqeSsA8KrozdOvxJ0UubrF6-HXjtkjI2XHOzpFLczS0yDdZ_2K0Obfv-DWzf7pubtH4YN8GmV',
//     'client_secret': 'EOXcmBC9XLG7KDzeS8NbDvl6ndppzJ1zzDqNpdIsrL0uz1V2wn1Ut37s4v1SJhIE7X4TS1gTjEyTKduP'
//   });

//   var newPayment = {
//       "intent": "sale",
//       "payer": {
//           "payment_method": "paypal"
//       },
//       "redirect_urls": {
//           "return_url": "https://instantwebtoapp.com",
//           "cancel_url": "https://instantwebtoapp.com"
//       },
//       "transactions": [{
//           "item_list": {
//               "items": [{
//                   "name": "Package VIP",
//                   "sku": "Unlimited build 10 apps",
//                   "price": "10.00",
//                   "currency": "USD",
//                   "quantity": 1
//               }]
//           },
//           "amount": {
//               "currency": "USD",
//               "total": "10.00"
//           },
//           "description": "Package VIP Unlimited build 10 apps."
//       }]
//   };

  
//   // return ;
//   await paypal.payment.create(newPayment, (error, payment) => {
//     if (error) {
//         throw error;
//     } else {
//         console.log("Create Payment Response");
//         console.log(payment);
//     }
//   });
// }

// payment();

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.status(404).json({
    'status': 'error',
    'messages': 'Not Found',
    'result': ''
  })
});

// error handler
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.status(err.status || 500).json({
    'status': 'error',
    'messages': res.locals.message,
    'result': res.locals.error
  })
});

module.exports = app;
