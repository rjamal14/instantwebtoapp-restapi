'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('app_builds', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      app_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      link: {
        allowNull: true,
        type: Sequelize.TEXT,
        defaultValue: null
      },
      status: {
        allowNull: false,
        type: Sequelize.ENUM('pending', 'queue', 'process', 'finish', 'error'),
      },
      type: {
        allowNull: false,
        type: Sequelize.ENUM('regular', 'premium', 'vip'),
        defaultValue: 'regular'
      },
      app_version: {
        allowNull: true,
        type: Sequelize.STRING,
        defaultValue: '1.0.0',
      },
      exp_date: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      createdAt: {
        type: Sequelize.DATE,
      },
      updatedAt: {
        type: Sequelize.DATE,
      },
      deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('app_builds');
  }
};