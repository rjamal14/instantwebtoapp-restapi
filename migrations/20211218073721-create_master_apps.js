'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('master_apps', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      domain_name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      screen_oriantation: {
        allowNull: false,
        type: Sequelize.ENUM('autorotate', 'lock_vertical', 'lock_horizontal'),
      },
      bg_color: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      bg_image: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      screen_icon: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      screen_position: {
        allowNull: false,
        type: Sequelize.ENUM('auto_height', 'auto_wide'),
      },
      snap_image: {
        allowNull: false,
        type: Sequelize.ENUM('top', 'center', 'bottom', 'left', 'right'),
      },
      app_icon: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      app_oriantation: {
        allowNull: false,
        type: Sequelize.ENUM('autorotate', 'lock_vertical', 'lock_horizontal'),
      },
      app_name: {
        allowNull: false,
        type: Sequelize.STRING, // TOP CENTER BOTTOM / LEFT CENTER RIGHT
      },
      app_descriptions: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      app_package: {
        allowNull: false,
        type: Sequelize.STRING, 
      },
      app_version: {
        allowNull: true,
        type: Sequelize.STRING,
        defaultValue: '1.0.0',
      },
      createdAt: {
        type: Sequelize.DATE,
      },
      updatedAt: {
        type: Sequelize.DATE,
      },
      deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('master_apps');
  }
};