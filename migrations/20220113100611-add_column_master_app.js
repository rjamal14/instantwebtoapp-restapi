'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.addColumn('master_apps', 'screen_animation_fade', { type: Sequelize.DOUBLE, defaultValue: 3});
    await queryInterface.addColumn('master_apps', 'app_position', { type: Sequelize.ENUM('auto_height', 'auto_wide'), allowNull: false})

  },

  down: async (queryInterface, Sequelize) => {

     await queryInterface.removeColumn('master_apps', 'screen_animation_fade');
     await queryInterface.removeColumn('master_apps', 'app_position');

  }
};
