'use strict';
const { Model } = require('sequelize');
const sequelizePaginate = require('sequelize-paginate');

module.exports = (sequelize, DataTypes) => {
  class app_build extends Model {
    static associate(models) {
      models.app_build.belongsTo(models.master_app, { foreignKey: 'app_id' })
    }
  };

  app_build.init({
    app_id: DataTypes.INTEGER,
    link: DataTypes.STRING,
    status: DataTypes.ENUM('pending', 'queue', 'process', 'finish', 'error'),
    type: DataTypes.ENUM('regular', 'premium', 'vip'),
    app_version: DataTypes.STRING,
    exp_date: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'app_build',
    tableName: 'app_builds',
    paranoid: true,
  });

  sequelizePaginate.paginate(app_build);

  return app_build;
};