'use strict';
const { Model } = require('sequelize');
const sequelizePaginate = require('sequelize-paginate');

module.exports = (sequelize, DataTypes) => {
  class master_app extends Model {
    static associate(models) {
      models.master_app.belongsTo(models.user, { foreignKey: 'user_id' })
      models.master_app.hasMany(models.app_build, { foreignKey: 'app_id', as: 'app_builds' })
    }
  };

  master_app.init({
    user_id: DataTypes.INTEGER,
    domain_name: DataTypes.STRING,
    screen_oriantation: DataTypes.ENUM('autorotate', 'lock_vertical', 'lock_horizontal'),
    bg_color: DataTypes.STRING,
    bg_image: DataTypes.TEXT,
    screen_icon: DataTypes.TEXT,
    screen_position: DataTypes.ENUM('auto_height', 'auto_height'),
    screen_animation_fade: DataTypes.DOUBLE,
    snap_image: DataTypes.ENUM('top', 'center', 'bottom', 'left', 'right'),
    app_icon: DataTypes.TEXT,
    app_oriantation: DataTypes.ENUM('autorotate', 'lock_vertical', 'lock_horizontal'),
    app_position: DataTypes.ENUM('auto_height', 'auto_height'),
    app_name: DataTypes.STRING,
    app_descriptions: DataTypes.TEXT,
    app_package: DataTypes.STRING,
    app_version: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'master_app',
    tableName: 'master_apps',
    paranoid: true,
  });

  sequelizePaginate.paginate(master_app);

  return master_app;
};