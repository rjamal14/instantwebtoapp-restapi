'use strict';
const { Model } = require('sequelize');
const sequelizePaginate = require('sequelize-paginate');

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    static associate(models) {
      models.user.hasOne(models.profile, { foreignKey: 'user_id', as: 'profile' })
      models.user.hasMany(models.master_app, { foreignKey: 'user_id', as: 'master_apps' })
    }
  };

  user.init({
    username: DataTypes.STRING,
    password: DataTypes.TEXT,
    role: DataTypes.ENUM('admin', 'user')
  }, {
    sequelize,
    modelName: 'user',
    tableName: 'users',
    paranoid: true,
  });

  sequelizePaginate.paginate(user);

  return user;
};