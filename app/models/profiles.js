'use strict';
const { Model } = require('sequelize');
const sequelizePaginate = require('sequelize-paginate');

module.exports = (sequelize, DataTypes) => {
  class profile extends Model {
    static associate(models) {
      models.profile.belongsTo(models.user, { foreignKey: 'user_id' })
    }
  };

  profile.init({
    user_id: DataTypes.INTEGER,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    full_name: DataTypes.STRING,
    sex: DataTypes.ENUM('male', 'female'),
    address: DataTypes.STRING,
    province_id: DataTypes.INTEGER,
    city_id: DataTypes.INTEGER,
    postal_code: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'profile',
    tableName: 'profiles',
    paranoid: true,
  });

  sequelizePaginate.paginate(profile);

  return profile;
};