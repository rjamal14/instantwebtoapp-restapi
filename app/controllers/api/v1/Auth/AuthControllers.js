const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
var jwt = require('jsonwebtoken');
var fs = require('fs');
var generator = require('generate-password');

// oauth token
var reqBodyToken = [
    check('username').isLength({ min: 1 }).withMessage('Not empty'),
    check('password').isLength({ min: 1 }).withMessage('Not empty')
];

router.post('/token', reqBodyToken, async(req, res, next) => {
    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                status: "error",
                message: (errors['errors'][0].param+" "+errors['errors'][0].msg),
                result: errors['errors'],
            });
        }

        var data = req.body
        var user = await verifyuser(data.username, data.password)
        
        if(user.status == true ){
            data = {
                type: "user",
                id: user.id
            }
            var cert = fs.readFileSync(appDir + '/config/key/private.key');
            var token = jwt.sign({ exp: Math.floor(Date.now() / 1000) + (60 * 60), data: data }, cert, { algorithm: 'RS512' });

            res.status(200).json({
                status: 'success',
                message: 'Successfully',
                result: token
            });
        }else{
            res.status(401).json({
                status: 'error',
                message: 'Unauthorized',
                result: [
                    { msg: user.message}
                ],
            });
        }
    } catch (error) {
        console.log(error)
        res.status(400).json({
            status: "error",
            message: error.name,
            result:error,
        });
    }
});

// oauth token client
var reqBodyTokenClient = [
    check('client_id').isLength({ min: 1 }).withMessage('Not empty'),
    check('client_secret').isLength({ min: 1 }).withMessage('Not empty')
];

router.post('/token/client', reqBodyTokenClient, async(req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                status: "error",
                message: (errors['errors'][0].param+" "+errors['errors'][0].msg),
                result: errors['errors'],
            });
        }

        var data = req.body
        var client = await verifyclient(data.client_id, data.client_secret);
        
        if(client.status == true){
            data = {
                type: "client_credential",
                id: client.id
            }
            var cert = fs.readFileSync(appDir + '/config/key/private.key');
            var token = jwt.sign({ exp: Math.floor(Date.now() / 1000) + (60 * 60), data: data }, cert, { algorithm: 'RS512' });
        
            res.status(200).json({
                status: 'success',
                message: 'Successfully',
                result: token
            });
        }else{
            res.status(401).json({
                status: 'error',
                message: 'Unauthorized',
                result: [
                    { msg: 'client credential not register'}
                ],
            });
        }

    } catch (error) {
        console.log(error);
        res.status(400).json({
            status: "error",
            message: error.name,
            result:error,
        });
    }
    

});

// client credential create
var reqBodyRegister = [
    check('first_name').isLength({ min: 1 }).withMessage('Not empty min 1 char'),
    check('last_name').isLength({ min: 1 }).withMessage('Not empty min 1 char'),
    check('email').isEmail().custom(async (email,{req}) => { 
        const existingUser =  await model.profile.findOne({
          where: { email: email },
        }) 
        if (existingUser) { 
          if (existingUser.id != req.params.id) { 
            throw new Error('already exists') 
          } 
        } 
    })
];

router.post('/register', reqBodyRegister, async(req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
            status: "error",
            message: (errors['errors'][0].param+" "+errors['errors'][0].msg),
            result: errors['errors'],
            });
        }

        const data = await LibUsers.createUser(req.body);

        res.status(200).json({
            status: 'success',
            message: 'Successfully',
            result: await data
        });

    } catch (error) {
        console.log(error);
        res.status(400).json({
            status: "error",
            message: error.name,
            result:error,
        });
    }

});

var ReqMagicLink = [
    check('email').isEmail().withMessage('must format email')
];

router.post('/magic-link', ReqMagicLink, async(req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
            status: "error",
            message: (errors['errors'][0].param+" "+errors['errors'][0].msg),
            result: errors['errors'],
            });
        }

        const user =  await model.user.findOne({
            include: [{
                model: model.profile,
                as: 'profile',
                where: { email: req.body.email },
            }]
        }) 


        if(!user){
            userData = await LibUsers.createUser(req.body);
        }

        await LibUsers.generateLink(user || userData);

        res.status(200).json({
            status: 'success',
            message: 'Successfully',
            result: []
        });

    } catch (error) {
        console.log(error);
        res.status(400).json({
            status: "error",
            message: error.name,
            result:error,
        });
    }

});



// client credential create
var reqBodyTokenCreate = [
    check('app_name').isLength({ min: 3 }).withMessage('Not empty min 4 char'),
];

router.post('/token/client/create', reqBodyTokenCreate, async(req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                status: "error",
                message: (errors['errors'][0].param+" "+errors['errors'][0].msg),
                result: errors['errors'],
            });
        }

        const oauth_credential = await generateClient('client', req.body.app_name)

        if (oauth_credential.status) {
            res.status(200).json({
                status: 'success',
                message: 'Successfully',
                result: oauth_credential.data
            });
        }

    } catch (error) {
        console.log(error)
        res.status(400).json({
            status: "error",
            message: error.name,
            result:error,
        });
    }

});

// verity JWT token
var reqBodyAuth = [
    check('token').isLength({ min: 1 }).withMessage('Not empty')
];

router.get('/auth', reqBodyAuth, (req, res, next) => {
    try {
        var cert_public = fs.readFileSync(appDir + '/config/key/public.key');
        var token = req.query.token;
        jwt.verify(token, cert_public, { algorithms: ['RS512'] }, function (err, payload) {
            if (!err) {
                res.status(200).json({
                    status: 'success',
                    message: 'Successfully',
                    result: payload,
                });
            } else {
                res.status(401).json({
                    status: 'error',
                    message: 'Unauthorized',
                    result: [{"msg":err}]
                });
            }
        });
    } catch (error) {
        console.log(error)
        res.status(400).json({
            status: "error",
            message: error.name,
            result:error,
        });
    }
});


// function
var verifyclient = async(client_id, client_credential) => {
    const client = await model.oauth_credential.findOne({ 
                    where: { 
                        client_id: client_id,
                        client_secret: client_credential 
                    }});

    if(client === null){
        return { message: 'client credential terdaftar', status: false}
    }else{
        return { message: 'Berhasil Login', status: true, id: client.id }
    }

}

// function

var verifyuser = async(username, password) => {
    const user = await model.user.findOne({ 
                    where: { username: username }});

    if(user === null){
        return { message: 'User tidak terdaftar', status: false}
    }else{
        if (decrypt(user.password) == password){
            return { message: 'Berhasil Login', status: true, id: user.id }
        }else{
            return { message: 'Password salah', status: false }
        }
    }

}

var generateClient = async(provider, attr, transaction) => {
    try {
        var oauth_data;
        const rsdp = Math.floor(Date.now() / 1000) + (60 * 60) + Math.floor(Math.random() * Math.floor(1000))

        if(provider == 'client'){
            oauth_data = {
                app_name: attr,
                client_id: encrypt(rsdp),
                client_secret: encrypt(rsdp),
                provider: provider
            }
        }else{
            oauth_data = {
                user_id: attr,
                client_id: encrypt(rsdp),
                client_secret: encrypt(rsdp),
                provider: provider
            }
        }

        const oauth_credential = await model.oauth_credential.create(oauth_data, {transaction: transaction});

        if(oauth_credential){
            return { message: 'Berhasil membuat oauth credential', status: true, data: oauth_credential }
        }else{
            return { message: 'Gagal membuat oauth credential', status: false }
        }
    } catch (error) {
        console.error(error);
        return { message: 'Gagal membuat oauth credential', status: false }
    }
    
}


module.exports = router;