const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');


router.get("/", auth(), async (req, res, next) => {
  try {

    const page = parseInt(req.query.page) || 1;
    const per_page = parseInt(req.query.per_page) || 10;

    var options = {
      page: page < 1 ? 1 : page,
      paginate: per_page,
      order: [["id", "desc"]],
    };
    const { docs, pages, total } = await model.app_build.paginate(options);

    if (docs) {
      res.status(200).json({
        status: "success",
        message: "Successfully",
        result: {
          data: await docs,
          currentPage: page,
          nextPage: page >= pages ? false : page + 1,
          totalItems: total,
          totalPages: pages,
        }
      });
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({
      status: "error",
      message: error.name,
      result:error,
    });
  }
});


router.get("/:id", auth(), async (req, res, next) => {
  try {

    var options = {
        where: { id: req.params.id }
    };

    const docs = await model.app_build.findOne(options);

    if (docs) {
      res.status(200).json({
        status: "success",
        message: "Successfully",
        result: await docs
      });
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({
      status: "error",
      message: error.name,
      result:error,
    });
  }
});


var reqBodyCreate = [
    check('app_id').isLength({ min: 1 }).withMessage('Not empty'),
    check('status').isLength({ min: 1 }).withMessage('Not empty'),
    check('type').isLength({ min: 1 }).withMessage('Not empty')
];

router.post('/create', auth(), reqBodyCreate, async(req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                status: "error",
                message: (errors['errors'][0].param+" "+errors['errors'][0].msg),
                result: errors['errors'],
            });
        }

        const {
            app_id,
            type
        } = req.body


        var exp_date = new Date();
        exp_date.setDate(exp_date.getDate() + (req.body.status=='premium') ? 360 : 7);


        const MasterApp =  await model.master_app.findOne({where: {id: req.body.app_id} })

        if(!MasterApp.app_version){
            res.status(400).json({
                status: "error",
                message: "App id not found",
                result: [],
            });
        }

        var app_version =  MasterApp.app_version
        app_version = (parseInt(app_version.replace(/\./g,''))+1);
        app_version = app_version.toString().split("").toString().replace(/\,/g,'.');

        const AppBuild = await model.app_build.create({
            app_id,
            status: req.body.status || 'pending',
            type,
            app_version: app_version,
            exp_date: exp_date
        });

        await MasterApp.update({app_version: app_version});

        res.status(200).json({
            status: 'success',
            message: 'Successfully',
            result: await AppBuild
        });

    } catch (error) {
        console.log(error);
        res.status(400).json({
            status: "error",
            message: error.name,
            result:error,
        });
    }
});


module.exports = router;