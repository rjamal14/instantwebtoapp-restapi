const express = require('express');
const router = express.Router();

// update profile
router.patch('/:id', auth(), async(req, res, next) => {
    var data = req.body

    const profile = await model.profile.findOne({where:{ user_id: req.params.id}})

    if(profile){
        await profile.update(data, { returning: true, plain: true }).then((profile)=>{
            res.status(200).json({
                status: 'success',
                message: 'Successfully',
                result: profile,
            });
        }).catch((error)=>{
            res.status(400).json({
                status: "error",
                message: error.name,
                result:error,
            });
        })
    }

});


module.exports = router;