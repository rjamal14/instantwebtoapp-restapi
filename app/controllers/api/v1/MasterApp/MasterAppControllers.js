const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');


router.get("/", auth(), async (req, res, next) => {
  try {

    const page = parseInt(req.query.page) || 1;
    const per_page = parseInt(req.query.per_page) || 10;

    var options = {
      include: [{  
          model: model.app_build, 
          as: 'app_builds' 
      }],
      page: page < 1 ? 1 : page,
      paginate: per_page,
      order: [["id", "desc"]],
    };
    const { docs, pages, total } = await model.master_app.paginate(options);

    if (docs) {
      res.status(200).json({
        status: "success",
        message: "Successfully",
        result: {
          data: await docs,
          currentPage: page,
          nextPage: page >= pages ? false : page + 1,
          totalItems: total,
          totalPages: pages,
        }
      });
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({
      status: "error",
      message: error.name,
      result:error,
    });
  }
});


router.get("/:id", auth(), async (req, res, next) => {
  try {

    var options = {
        where: { id: req.params.id },
        include: [{  
            model: model.app_build, 
            as: 'app_builds' 
        }]
    };
    const docs = await model.master_app.findOne(options);

    if (docs) {
      res.status(200).json({
        status: "success",
        message: "Successfully",
        result: await docs
      });
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({
      status: "error",
      message: error.name,
      result:error,
    });
  }
});


var reqBodyCreate = [
    check('email').isLength({ min: 1 }).withMessage('Not empty'),
    check('domain_name').isLength({ min: 1 }).withMessage('Not empty'),
    check('screen_oriantation').isLength({ min: 1 }).withMessage('Not empty'),
    check('bg_color').isLength({ min: 1 }).withMessage('Not empty'),
    check('screen_position').isLength({ min: 1 }).withMessage('Not empty'),
    check('screen_animation_fade').isLength({ min: 1 }).withMessage('Not empty'),
    check('snap_image').isLength({ min: 1 }).withMessage('Not empty'),
    check('app_icon').isLength({ min: 1 }).withMessage('Not empty'),
    check('app_name').isLength({ min: 1 }).withMessage('Not empty'),
    check('app_oriantation').isLength({ min: 1 }).withMessage('Not empty'),
    check('app_position').isLength({ min: 1 }).withMessage('Not empty'),
    check('app_descriptions').isLength({ min: 1 }).withMessage('Not empty'),
    check('app_package').isLength({ min: 1 }).withMessage('Not empty')
];

router.post('/create', reqBodyCreate, async(req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                status: "error",
                message: (errors['errors'][0].param+" "+errors['errors'][0].msg),
                result: errors['errors'],
            });
        }

        var data = req.body
        var user;

        const profile = await model.profile.findOne({where:{ email: data.email}})

        if(!profile){
            user = await LibUsers.createUser(req.body);
        }else{
            user = await model.user.findOne({where:{ id: profile.user_id}})
        }

        data.user_id = await user.id;
        var exp_date = new Date();
        exp_date.setDate(exp_date.getDate() + 7);

        const MasterApp = await model.master_app.create(data);
        const AppBuild = await model.app_build.create({
            app_id: MasterApp.id,
            status: 'pending',
            type: 'regular',
            app_version: '1.0.0',
            exp_date: exp_date
        });

        const bg_image = await uploadImage(req.body.bg_image, 'bg_image', await MasterApp.id);
        const screen_icon = await uploadImage(req.body.screen_icon, 'screen_icon', await MasterApp.id);
        const app_icon = await uploadImage(req.body.app_icon, 'app_icon', await MasterApp.id);
        
        await MasterApp.update({
            bg_image : await bg_image,
            screen_icon: await screen_icon,
            app_icon: await app_icon
        })

        res.status(200).json({
            status: 'success',
            message: 'Successfully',
            result: {...await MasterApp.get(), app_builds: AppBuild}
        });

    } catch (error) {
        console.log(error);
        res.status(400).json({
            status: "error",
            message: error.name,
            result:error,
        });
    }
});


router.patch('/update/:id', auth(), async (req, res, next) => {
    try {

        var options = {
          where: { id: req.params.id },
          include: [{  
              model: model.app_build, 
              as: 'app_builds' 
          }]
        };

        const data = req.body
        const MasterApp = await model.master_app.findOne(options);

        data.bg_image = await uploadImage(req.body.bg_image, 'bg_image', await MasterApp.id);
        data.screen_icon = await uploadImage(req.body.screen_icon, 'screen_icon', await MasterApp.id);
        data.app_icon = await uploadImage(req.body.app_icon, 'app_icon', await MasterApp.id);

        await MasterApp.update(data);

        res.status(200).json({
            status: 'success',
            message: 'Successfully',
            result: await MasterApp.get()
        });

    } catch (error) {
        console.log(error);
        res.status(400).json({
            status: "error",
            message: error.name,
            result:error,
        });
    }
});


module.exports = router;