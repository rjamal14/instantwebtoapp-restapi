module.exports = {
 apps: [
  {
   name: "instantwebtoapp-restapi",
   script: "./bin/www",
   env: {
    PORT: 3000,
    NODE_ENV: "production"
   },
   env_production : {
    "NODE_ENV": "production"
   }
  }
 ],
 deploy: {
  production: {
   user: "deploy",
   host: "34.124.149.91",
   key: "./config/key/deploy.pem",
   ref: "origin/production",
   repo: "git@bitbucket.org:instantwebtoapp/instantwebtoapp-restapi.git",
   path: "/home/deploy/instantwebtoapp-restapi",
   env: { "NODE_ENV": "production" },
   "pre-setup": "rm -rf /home/deploy/instantwebtoapp-restapi/source",
   "post-setup": "npm install; cp -a /home/deploy/instantwebtoapp-restapi/shared/config/ /home/deploy/instantwebtoapp-restapi/source/; cp /home/deploy/instantwebtoapp-restapi/shared/.env /home/deploy/instantwebtoapp-restapi/source/.env; sequelize db:migrate --env production;",
   "post-deploy": "pm2 startOrRestart ecosystem.config.js --env production"
  }
 }
};